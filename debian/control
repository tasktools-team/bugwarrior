Source: bugwarrior
Maintainer: Debian Tasktools Team <team+tasktools@tracker.debian.org>
Uploaders: Jochen Sprickerhof <jspricke@debian.org>
Section: utils
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-sequence-python3,
               dh-sequence-sphinxdoc,
               python3-all,
               python3-bugzilla <!nocheck>,
               python3-click <!nocheck>,
               python3-dateutil <!nocheck>,
               python3-dogpile.cache <!nocheck>,
               python3-googleapi <!nocheck>,
               python3-google-auth-oauthlib <!nocheck>,
               python3-jinja2 <!nocheck>,
               python3-jira <!nocheck>,
               python3-lockfile <!nocheck>,
               python3-offtrac <!nocheck>,
               python3-pytest <!nocheck>,
               python3-requests <!nocheck>,
               python3-responses <!nocheck>,
               python3-setuptools,
               python3-six <!nocheck>,
               python3-taskw <!nocheck>,
               python3-tz <!nocheck>,
               python3-xdg <!nocheck>,
               python3-debianbts <!nocheck>,
               python3-sphinx,
               python3-phabricator <!nocheck>,
Rules-Requires-Root: no
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/tasktools-team/bugwarrior
Vcs-Git: https://salsa.debian.org/tasktools-team/bugwarrior.git
Homepage: https://github.com/ralphbean/bugwarrior
Testsuite: autopkgtest-pkg-pybuild

Package: bugwarrior
Architecture: all
Depends: ${python3:Depends},
         ${misc:Depends},
         ${sphinxdoc:Depends}
Recommends: python3-pypandoc,
            python3-keyring,
            python3-phabricator,
            python3-googleapi,
            python3-google-auth-oauthlib,
            python3-jira,
Built-Using: ${sphinxdoc:Built-Using}
Description: Pull tickets from bug trackers into taskwarrior
 bugwarrior is a command line utility for updating your local TaskWarrior
 database from your forge issue trackers.
 .
 It currently supports the following remote resources: ActiveCollab, Bitbucket,
 Bugzilla, Debian, Gerrit, GitHub, GitLab, Gmail, Jira, Megaplan, Pagure,
 Phabricator, Pivotal Tracker, RedMine, Taiga, TeamLab, Trac, Trello,
 Versionone, YouTrack.
